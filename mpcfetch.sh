#!/bin/bash
# This script checks to see if a new Maximum PC PDF issue is available.
# If it is, it downloads it and emails it.
# The file naming convention is: MPC_<yyyy>_<mm)-web.pdf

# Define the filename to look for and store it as a variable. 
CURRENTISSUE=MPC_$(date +%Y)_$(date +%m)-web.pdf

# Define the variable to use for exit code status.
STATUS=$?

#Try to fetch the file.
wget -P ./tmppdf http://dl.maximumpc.com/Archive/$CURRENTISSUE

if [ $STATUS eq 0 ]; then
   # Compress downloaded file
   gzip ./tmppdf/$CURRENTISSUE
   # Email file as attachment. 
   mutt -s "New Maximum PC Issue For $(date +%B) is available and attached" -a ./tmppdf/$CURRENTISSUE.gz < ./emailtmpl.txt

else

fi
